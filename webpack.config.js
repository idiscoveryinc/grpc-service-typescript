 // Common Webpack configuration used by webpack.config.development and webpack.config.production
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const production = /production/i.test(process.env.NODE_ENV);
const development = /development/i.test(process.env.NODE_ENV);
const test = /test/i.test(process.env.npm_lifecycle_event);
console.log('Service webpack.config NODE_ENV=' + process.env.NODE_ENV +
', HOST=' + process.env.HOST +
', PORT=' + process.env.PORT +
', lifecycle=' + process.env.npm_lifecycle_event +
', isProduction=', production,
', isTest=', test);

const config = {
  cache: true,
  devtool: 'source-map',
  mode: production ? 'production' : 'development',
  target: 'node', // in order to ignore built-in modules like path, fs, etc.
  externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
  entry: {
    cli: './src/client/cli.ts',
    server: './index.ts',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, './dist'),
    publicPath: '/'
  },
  devServer: {
    contentBase: './dist',
    hot: true
  },
  resolve: {
    modules: [
      path.join(__dirname, './src'),
      'node_modules'
    ],
    extensions: ['.js', '.ts', '.json', '.proto'],
    alias: {
      '@client': path.join(__dirname, './src/client/'),
      '@generated': path.join(__dirname, './generated/'),
      '@proto': path.join(__dirname, './src/proto/'),
      '@services': path.join(__dirname, './src/services/'),
      '@util': path.join(__dirname, './src/util/')
    }
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, './src/proto/*.proto'),
        to: '.'
      }
    ])
  ],
  module: {
    noParse: /\.min\.js$/,
    rules: [
      // TypeScript
      {
        test: /\.ts?$/,
        loader: 'ts-loader',
        exclude: [
          /node_modules/
        ]
      },
      {
        test: /\.proto$/,
        loader: 'grpc-loader',
        options: {
          // both methods are static internally
          // but result api differs
          // https://github.com/grpc/grpc/tree/master/examples/node
          static: false,

          // grpc props
          // https://github.com/grpc/grpc-node/blob/master/packages/grpc-protobufjs/index.js#L37-L42
          //  convertFieldsToCamelCase,
          //  binaryAsBase64,
          //  longsAsStrings,
          //  enumsAsStrings,
        },
      }
    ]
  },
};

module.exports = config;
