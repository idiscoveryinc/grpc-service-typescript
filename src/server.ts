/**
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import logger from '@util/logger';
import * as grpc from 'grpc';

const context = (require as any).context('./services', true, /\.ts$/);
const modules = context.keys().map(context);
const services = modules.map((module) => ({
    definition: module.default.definition,
    instance: new module.default(),
}));

const server = new grpc.Server();

const gracefulExit = () => {
    logger.info('Shutting down service...');
    process.exit();
};

process.on('SIGTERM', gracefulExit); // listen for TERM signal (e.g. kill)
process.on('SIGINT', gracefulExit); // listen for INT signal (e.g. Ctrl-C)

services.forEach((service) => {
    server.addService(service.definition, service.instance);
});

server.bind(`${process.env.HOST}:${process.env.PORT}`,
            grpc.ServerCredentials.createInsecure());

export default server;
