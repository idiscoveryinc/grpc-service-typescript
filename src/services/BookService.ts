/**
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import { books } from '@generated/messages';
import { ServerUnaryCall, ServerWriteableStream } from 'grpc';
// tslint:disable-next-line:no-var-requires
const proto = require('@proto/Books.proto');

export default class BookService {
    static definition = proto.books.BookService.service;
    books: books.IBook[] = [{
        id: 123,
        name: 'My Group',
        author: 'Robert',
    }];
    bookStream: ServerWriteableStream<books.IBook>;

    list(request: books.IEmpty, callback: books.BookService.ListCallback) {
        callback(null, new books.BookList({ groups: this.books }));
    }

    insert(call: ServerUnaryCall<books.IBook>, callback: books.BookService.InsertCallback) {
        const { request } = call;

        this.books.push(request);
        if (this.bookStream) {
            this.bookStream.write(request);
        }
        callback(null, new books.Empty());
    }

    get(request: books.IBookIdRequest, callback: books.BookService.GetCallback) {
        const book = this.books.find((book: books.IBook) => book.id === request.id);
        callback(null, new books.Book(book));
    }

    delete(request: books.IBookIdRequest, callback: books.BookService.DeleteCallback) {
        const index = this.books.findIndex((book: books.IBook) => book.id === request.id);
        this.books.splice(index, 1);
        callback(null, new books.Empty());
    }

    watch(stream: ServerWriteableStream<books.IBook>) {
        this.bookStream = stream;
        return this.bookStream;
    }
}
