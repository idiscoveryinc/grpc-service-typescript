/**
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import * as messages from '@generated/messages';
import logger from '@util/logger';
import * as grpc from 'grpc';

// tslint:disable-next-line:no-var-requires
const proto = require('@proto/Sample.proto');
const serviceAddress = `${process.env.HOST}:${process.env.PORT}`;

logger.info(`SampleServiceClient connecting to service address ${serviceAddress}`);

const service = new proto.sample.SampleService(
        serviceAddress,
        grpc.credentials.createInsecure()) as messages.sample.SampleService;

export const doSomething = () => {
    logger.debug('doing something operation');
    return new Promise((resolve, reject) => {
        const request = new messages.sample.SampleServiceRequest({
            logFilePath: 'file path',
            portNumber: 32,
        });

        service.execute(request, (error: Error, response: messages.sample.SampleServiceReply) => {
            if (error) { reject(error); }
            resolve(response);
        });
    });
};
