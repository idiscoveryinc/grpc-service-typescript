/**
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import { books } from '@generated/messages';
import logger from '@util/logger';
import * as grpc from 'grpc';
// tslint:disable-next-line:no-var-requires
const proto = require('@proto/Books.proto');
const serviceAddress = `${process.env.HOST}:${process.env.PORT}`;

logger.info(`BookServiceClient connecting to service address ${serviceAddress}`);

const service = new proto.books.BookService(serviceAddress, grpc.credentials.createInsecure()) as books.BookService;

export const listBooks = () => {
    return new Promise((resolve, reject) => {
        service.list({}, (error, response) => {
            if (error) { reject(error); }
            resolve(response);
        });
    });
};

export const insertBook = (id: number, name: string, author: string): Promise<books.IEmpty> => {
    const book = {
        id,
        name,
        author,
    };
    return new Promise((resolve, reject) => {
        service.insert(book, (error, response) => {
            if (error) { reject(error); }
            resolve(response);
        });
    });
};

export const getBook = (id) => {
    return service.get({
        id: parseInt(id, 10),
    });
};

export const deleteBook = (id) => {
    return service.delete({
        id: parseInt(id, 10),
    });
};

export const watchBooks = (): grpc.ClientReadableStream<books.IBook> => {
    const call = service.watch({});

    return (call as any) as grpc.ClientReadableStream<books.IBook>;
};
