/**
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import logger from '@util/logger';
import * as program from 'commander';
import { deleteBook, getBook, insertBook, listBooks, watchBooks } from './BookServiceClient';
import { doSomething } from './SampleServiceClient';

program
  .version('0.0.1')
  .option('-i, --insert [name]', 'Insert a Book [name]')
  .option('-g, --get [id]', 'Get a Book [id]')
  .option('-d, --delete [id]', 'Delete a Book [id]')
  .option('-l, --list', 'List Books')
  .option('-w, --watch', 'Watch Books')
  .option('-s, --something', 'Do something')
  .parse(process.argv);

if (program.insert) {
    insertBook(parseInt(process.argv[3], 10), process.argv[4], process.argv[5]);
}
else if (program.get) {
    getBook(process.argv[0]);
}
else if (program.delete) {
    deleteBook(process.argv[0]);
}
else if (program.list) {
    listBooks().then((books) => logger.debug(JSON.stringify(books)), (error) => logger.error(error));
}
else if (program.watch) {
    watchBooks();
}
else if (program.ssh) {
    doSomething();
}
