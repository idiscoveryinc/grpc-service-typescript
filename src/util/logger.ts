/**
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import * as fs from 'fs';
import * as path from 'path';
import * as winston from 'winston';

const { combine, timestamp, label, printf } = winston.format;
const outputDir = path.join(process.cwd(), './logs');

if (!fs.existsSync(outputDir)) {
  fs.mkdirSync(outputDir);
}

const format = printf((info) => {
  return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
});

const logger = winston.createLogger({
  level: 'debug',
  format: combine(
    winston.format.colorize(),
    label({ label: `${process.env.HOST}:${process.env.PORT}` }),
    timestamp(),
    format,
  ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({
      filename: `${outputDir}/error.log`,
      level: 'error',
    }),
    new winston.transports.File({
      filename: `${outputDir}/all.log`,
      maxsize: 1024 * 1024 * 10, // 10MB
    }),
  ],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console());
}

export default logger;
