# iDiscovery Service Template

Service boilerplate for building Node grpc services.

## Development

### Build

Run the following commands:
* `npm install` will install dependencies
* `npm run start:dev` builds the project and starts the grpc service

To use the cli:
* `npm run cli -- --list` executes the client with args after --

### Static Analysis

TsLint is used for static analysis. The configuration can be found in `tslint.json`.

To run tslint:
* `npm run lint`

### Tests

Tests are built with the Mocha test framework and Chai assertion library.

Place all tests in the `spec/` directory with the suffix `.spec.ts`.

To run tests:
* `npm test`
* `npm run test:watch`

### Message generation

Messages are dynamically generated at build time using protobufjs.

To invoke code generation:
* `npm run build:proto`

### Logging

Logging is handled by Winston. For more info see https://www.npmjs.com/package/winston


## Services

Services are dynamically added to the grpc server. To facilitate this, each service must have a `static definition` property that references the protobuf service definition.