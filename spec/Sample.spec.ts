/**
 * @author Robert Hamilton rhamilton@navaris.com
 *
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import { doSomething } from '@client/SampleServiceClient';
import { sample } from '@generated/messages';
import logger from '@util/logger';
import * as chai from 'chai';
import './Helper';

describe('SampleService', () => {
  describe('doSomething operation', () => {
    it('should do something', () => {
      chai.assert.equal([1, 2, 3].indexOf(4), -1);
      return doSomething().then((response: sample.SampleServiceReply) => {
        chai.assert.equal(response.note, 'hello world', 'Say hello');
      }, (error) => logger.error(error));
    });
  });
});
