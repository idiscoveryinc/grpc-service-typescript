/**
 * @author Robert Hamilton rhamilton@navaris.com
 *
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import { insertBook, listBooks, watchBooks } from '@client/BookServiceClient';
import { books } from '@generated/messages';
import logger from '@util/logger';
import * as chai from 'chai';
import './Helper';

const bookList: books.IBook[] = [
  { id: 1, name: 'Design Principles', author: 'Gang of Four' },
  { id: 2, name: 'The Pragmatic Programmer', author: 'Andrew Hunt' },
  { id: 3, name: 'Javascript: The Good Parts', author: 'Douglas Crockford' },
];

describe('BookService', () => {
  describe('listBooks operation', () => {
    it('should return a list of books', () => {
      chai.assert.equal([1, 2, 3].indexOf(4), -1);
      return listBooks().then((books: books.BookList) => {
        chai.assert.equal(books.groups.length, 1, 'Return a book list of length 1');
      }, (error) => logger.error(error));
    });
  });

  describe('Watch operation', () => {
    it('should notify when a book is added', () => {
      let count = 0;
      return new Promise((resolve, reject) => {
        watchBooks().on('data', (book) => {
          count++;
          chai.assert.isNotEmpty(bookList.find((item) => item.id === book.id), 'Books should be in collection.');
          if (count === bookList.length) {
            resolve();
          }
        });

        bookList.forEach((book) => {
          const { id, name, author } = book;
          insertBook(id, name, author);
        });
      });
    });
  });
});
