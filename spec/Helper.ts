/**
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

import logger from '@util/logger';
import server from '../src/server';

before(() => {
  server.start();
  logger.info(`Service is listening on ${process.env.HOST}:${process.env.PORT}`);
});

after(() => {
  server.tryShutdown(() => logger.info('Service shutdown'));
});
