/**
 * Copyright 2017 iDiscovery, Inc. All rights reserved
 *
 * NOTICE:  All information contained herein is, and remains the property of Navaris Technologies, LLC.  The intellectual and technical concepts
 * contained herein are proprietary to Navaris Technologies, LLC and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.  Dissemination of this information or reproduction of this material
 * is strictly forbidden, unless prior authorized written permission is obtained from Navaris Technologies, LLC.
 */

// import * as path from 'path';
import logger from '@util/logger';
import server from './src/server';

// if (process.env.NODE_ENV === 'development') {
//   const webpackDevServer = require('webpack-dev-server');
//   const webpack = require('webpack');
//
//   const config = require('./webpack.config.js');
//   const options = {
//     contentBase: path.join(__dirname, './dist'),
//     hot: true,
//     host: 'localhost',
//     watchContentBase: true,
//   };
//
//   webpackDevServer.addDevServerEntrypoints(config, options);
//   const compiler = webpack(config);
//   const server = new webpackDevServer(compiler, options);
//
//   server.listen(5000, 'localhost', () => {
//     console.log('dev server listening on port 5000');
//   });
// }

// if ((module as any).hot) {
//   (module as any).hot.accept();
// }

server.start();
logger.info(`Service is listening on ${process.env.HOST}:${process.env.PORT}`);
